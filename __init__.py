import bl00mbox

from st3m.application import Application, ApplicationContext
from st3m.input import InputState
from ctx import Context
from st3m.ui.view import View, ViewManager

from st3m.application import Application, ApplicationContext
from st3m.ui.view import BaseView, ViewManager
from st3m.input import InputState
from ctx import Context

class CAPITAN(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self.blm = bl00mbox.Channel("CAPITAN")
        self.cap = self.blm.new(bl00mbox.patches.sampler, f"{self.app_ctx.bundle_path}/CAPITAN.wav")
        self.image_path = f"{self.app_ctx.bundle_path}/capitan.png"
        self.cap.signals.output = self.blm.mixer
        self.cap.signals.trigger.start()

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        if self.input.captouch.petals[5].whole.pressed:
            self.cap.signals.trigger.start()

    def draw(self, ctx: Context) -> None:
        ctx.image(
            self.image_path,
            -120,
            -120,
            240,
            240,
        )



# uncomment to make runnable via mpremote
# st3m.run.run_view(Clouds(ApplicationContext()))
